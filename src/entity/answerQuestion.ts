import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class AnswerQuestion extends BaseEntity {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'uuid' })
  userUuid: string;

  @Column({ type: 'uuid', unique: true })
  answerUuid: string;

  @Column({ type: 'uuid', unique: true })
  questionUuid: string;
}
