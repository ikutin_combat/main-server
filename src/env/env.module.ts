import { Module } from '@nestjs/common';
import { DynamicModule } from '@nestjs/common';
import { Utilities } from 'src/utilities/utilities';

@Module({})
export class EnvModule {
  static register(): DynamicModule {
    const env = {
      provide: 'ENV',
      useValue: Utilities.getEnvironment(),
    };
    return {
      module: EnvModule,
      global: true,
      providers: [env],
      exports: [env],
    };
  }
}
