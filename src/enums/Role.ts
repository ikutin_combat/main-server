export enum Role {
  Admin = 'admin',
  User = 'user',
  SuperAdmin = 'superadmin',
}

export enum SpecificRoles {
  /**авторизованный */
  AUTHORIZED = 'authorized',
  /**проверка прав в самом методе */
  DEFERRED = 'deferred',
}
