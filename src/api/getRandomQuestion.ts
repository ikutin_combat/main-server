import axios from 'axios';
import { Environments } from 'src/utilities/utilities';

export const getRandomQuestion = async (
  env: Pick<Environments, 'QUESTION_SERVER_HOST' | 'QUESTION_SERVER_PORT'>,
) => {
  return (
    await axios.get(
      `${env.QUESTION_SERVER_HOST}:${env.QUESTION_SERVER_PORT}/api/questions/0/10`,
    )
  )?.data;
};
