import axios from 'axios';
import { CheckJwtBody } from 'src/models/checkJwtBody.model';
import { CheckJwtRes } from 'src/models/checkJwtRes.model';
import { Environments } from 'src/utilities/utilities';

export const checkJwt = async (
  obj: CheckJwtBody,
  env: Pick<Environments, 'AUTH_SERVER_HOST' | 'AUTH_SERVER_PORT'>,
): Promise<CheckJwtRes> => {
  return (
    await axios.post(
      `${env.AUTH_SERVER_HOST}:${env.AUTH_SERVER_PORT}/auth/checkJwt`,
      obj,
      {
        headers: {
          'Content-Type': 'application/json',
        },
      },
    )
  )?.data;
};
