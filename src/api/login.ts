import axios from 'axios';
import { loginBody } from 'src/models/loginBody.model';
import { loginRes } from 'src/models/loginRes.model';
import { Environments } from 'src/utilities/utilities';

export const login = async (
  obj: loginBody,
  env: Pick<Environments, 'AUTH_SERVER_HOST' | 'AUTH_SERVER_PORT'>,
): Promise<loginRes> => {
  return (
    await axios.post(
      `${env.AUTH_SERVER_HOST}:${env.AUTH_SERVER_PORT}/auth/login`,
      obj,
      {
        headers: {
          'Content-Type': 'application/json',
        },
      },
    )
  ).data;
};
