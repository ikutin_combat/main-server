import {
  Injectable,
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Role, SpecificRoles } from 'src/enums/Role';
import { AuthService } from 'src/auth/auth.service';
import { RequestUser } from 'src/middlewares/auth.middleware';

@Injectable()
export class ActionGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly authService: AuthService,
  ) {}
  async canActivate(context: ExecutionContext) {
    const roles = this.reflector.getAllAndOverride<(Role | SpecificRoles)[]>(
      'Roles',
      [context.getHandler(), context.getClass()],
    );
    try {
      if (typeof roles !== 'undefined' && roles.length > 0) {
        // суперадмин может все
        roles.push(Role.SuperAdmin);
      } else return true;
      const request = context.switchToHttp().getRequest<RequestUser>();
      if (roles.includes(SpecificRoles.AUTHORIZED) && request.user) return true;
      if (request.user?.error) throw request.user.error;
      if (request.user && roles.includes(request.user.role)) return true;
      if (roles.includes(SpecificRoles.DEFERRED)) return true;

      return false;
    } catch (e) {
      throw new HttpException(
        { status: HttpStatus.FORBIDDEN, error: e.message },
        HttpStatus.FORBIDDEN,
      );
    }
  }
}
