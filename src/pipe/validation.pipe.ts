import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  BadRequestException,
} from '@nestjs/common';
import { validate, ValidationError } from 'class-validator';
import { plainToClass } from 'class-transformer';
import e from 'express';

interface ValidationPipeConfig {
  array?: {
    type: { new () };
  };
}
interface ValidateErrorArray {
  errors: ValidationError[];
  position?: number;
}

@Injectable()
export class ValidationPipe<T> implements PipeTransform<any> {
  private config: ValidationPipeConfig;

  constructor(config?: ValidationPipeConfig) {
    this.config = config;
  }

  async transform(value: T, { metatype }: ArgumentMetadata) {
    let errors: ValidateErrorArray[] = [];
    if (this.config) {
      if (this.config.array) {
        if (Array.isArray(value)) {
          for (const i in value) {
            const object = plainToClass(this.config.array.type, value[i]);
            const validateRes = await validate(object);

            if (validateRes.length > 0) {
              errors.push({
                errors: validateRes,
                position: i,
              } as unknown as ValidateErrorArray);
            }
          }
        } else {
          throw new BadRequestException('Body must be array');
        }
      }
    } else {
      const object = plainToClass(metatype, value);
      errors = [{ errors: await validate(object) }];
    }

    if (!Array.isArray(value) && errors[0].errors.length > 0) {
      const errorsData = errors.map((p) => [
        ...p.errors.map((e) => ({
          target: e.property,
          errors: Object.values(e.constraints),
        })),
      ]);

      throw new BadRequestException(errorsData);
    }
    if (Array.isArray(value) && errors.length > 0) {
      const errorsData = errors.map((p) => [
        {
          ...p.errors.map((e) => ({
            target: e.property,
            errors: Object.values(e.constraints),
          })),
          position: +p.position ?? undefined,
        },
      ]);

      throw new BadRequestException(errorsData);
    }
    return value;
  }
}
