import { Role } from 'src/enums/Role';

export class loginRes {
  uuid: string;
  access_token: string;
  expiration: number;
}
