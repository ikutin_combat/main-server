import { IsUUID } from 'class-validator';

export class TestSaveBody {
  @IsUUID()
  answerUuid: string;

  @IsUUID()
  questionUuid: string;
}
