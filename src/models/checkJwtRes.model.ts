import { Role } from 'src/enums/Role';

export class CheckJwtRes {
  username: string;
  sub: string;
  role: Role;
  iat: number;
  exp: number;
}
