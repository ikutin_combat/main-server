import { Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction } from 'express';
import { AuthService } from 'src/auth/auth.service';
import { Response, Request } from 'express';

interface userError extends AsyncReturnType<AuthService['checkJwt']> {
  error: Error;
}

export interface RequestUser extends Request {
  user: userError | null;
}

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(private readonly authService: AuthService) {}
  async use(req: Request, res: Response, next: NextFunction) {
    const authorization = (req.cookies as any).authorization as string;
    if (typeof (req as any).user === 'undefined') {
      (req as any).user = null;
    }
    if (authorization) {
      const token = authorization;
      if (token) {
        try {
          const user = await this.authService.checkJwt(token);
          (req as any).user = user;
        } catch (e) {
          (req as any).user.error = e;
        }
      }
    } else {
      (req as any).user = null;
    }
    next();
  }
}
