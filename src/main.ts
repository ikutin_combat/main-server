import { NestFactory } from '@nestjs/core';
import { AppModule } from 'src/app/app.module';
import { Utilities } from 'src/utilities/utilities';
import * as cookieParser from 'cookie-parser';
/**
 * Авторизация +
 * Аунтификация +
 * Получение инфы о юзере
 * Создание юзера
 * Редактивание юзера
 * Получение вопросов +
 * Сохранение ответов
 * Получение статистики ответов
 * Админ может смотреть статистики других юзеров и ответов
 * Отдача меню в соответствии с правами
 * Просмотр результатов всех юзеров
 */

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(cookieParser());
  await app.listen(Utilities.getEnvironment().SERVER_PORT);
}

bootstrap();
