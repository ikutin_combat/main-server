import { SetMetadata } from '@nestjs/common';
import { Role, SpecificRoles } from 'src/enums/Role';

export const addRoles = (...roles: (Role | SpecificRoles)[]) =>
  SetMetadata('Roles', roles);
