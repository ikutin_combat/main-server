import { HttpException, Inject, Injectable } from '@nestjs/common';
import { getRandomQuestion } from 'src/api/getRandomQuestion';
import { Environments } from 'src/utilities/utilities';

@Injectable()
export class QuestionService {
  constructor(@Inject('ENV') private readonly env: Environments) {}

  async getRandom() {
    try {
      return await getRandomQuestion(this.env);
    } catch (e) {
      throw new HttpException(e.response.data, e.response.data.statusCode);
    }
  }
}
