import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { AuthModule } from 'src/auth/auth.module';
import { DbModule } from 'src/config/db.config';
import { EnvModule } from 'src/env/env.module';
import { ActionGuard } from 'src/guards/actionGuard';
import { AuthMiddleware } from 'src/middlewares/auth.middleware';
import { QuestionModule } from 'src/question/question.module';
import { TestModule } from 'src/test/test.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [
    DbModule.register(),
    EnvModule.register(),
    AuthModule,
    QuestionModule,
    TestModule,
  ],
  providers: [AppService, { provide: APP_GUARD, useClass: ActionGuard }],
  controllers: [AppController],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .forRoutes({ path: '*', method: RequestMethod.ALL });
  }
}
