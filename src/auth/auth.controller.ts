import {
  Body,
  Controller,
  HttpCode,
  Post,
  Res,
  UnauthorizedException,
} from '@nestjs/common';
import { loginBody } from 'src/models/loginBody.model';
import { AuthService } from './auth.service';
import { Response } from 'express';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  @HttpCode(200)
  async login(
    @Body() body: loginBody,
    @Res({ passthrough: true }) res: Response,
  ) {
    if (!body.username || !body.password) {
      throw new UnauthorizedException();
    }

    try {
      const login = await this.authService.login(body.username, body.password);
      res.cookie('authorization', login.access_token, {
        httpOnly: true,
      });
    } catch (e) {
      res.cookie('authorization', '', {
        maxAge: -1,
      });
      throw e;
    }

    return 'OK';
  }
}
