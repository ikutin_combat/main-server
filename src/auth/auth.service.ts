import { HttpException, Inject, Injectable } from '@nestjs/common';
import { checkJwt } from 'src/api/checkJwt';
import { login } from 'src/api/login';
import { loginRes } from 'src/models/loginRes.model';
import { Environments } from 'src/utilities/utilities';

@Injectable()
export class AuthService {
  constructor(@Inject('ENV') private readonly env: Environments) {}

  async login(username: string, password: string): Promise<loginRes> {
    try {
      return await login({ username, password }, this.env);
    } catch (e) {
      throw new HttpException(e.response.data, e.response.data.statusCode);
    }
  }

  async checkJwt(jwt: string) {
    try {
      return await checkJwt({ jwt }, this.env);
    } catch (e) {
      throw new HttpException(e.response.data, e.response.data.statusCode);
    }
  }
}
