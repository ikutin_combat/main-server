import {
  Body,
  Controller,
  HttpCode,
  Post,
  Req,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { addRoles } from 'src/decorators/addRoles';
import { AnswerQuestion } from 'src/entity/answerQuestion';
import { SpecificRoles } from 'src/enums/Role';
import { RequestUser } from 'src/middlewares/auth.middleware';
import { TestSaveBody } from 'src/models/testSaveBody';
import { ValidationPipe } from 'src/pipe/validation.pipe';
import { TestService } from './test.service';

@Controller('test')
export class TestController {
  constructor(private readonly testService: TestService) {}

  @Post('save')
  @HttpCode(200)
  @addRoles(SpecificRoles.AUTHORIZED)
  async save(
    @Req() req: RequestUser,
    @Body(new ValidationPipe({ array: { type: TestSaveBody } }))
    body: TestSaveBody[],
  ) {
    try {
      await this.testService.save(req.user.sub, body);
    } catch (e) {
      throw new HttpException(
        {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: 'something happened',
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }

    return 'll';
  }
}
