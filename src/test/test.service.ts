import { Injectable } from '@nestjs/common';
import { AnswerQuestion } from 'src/entity/answerQuestion';
import { TestSaveBody } from 'src/models/testSaveBody';

@Injectable()
export class TestService {
  async save(userUuid: string, data: TestSaveBody[]) {
    const answerQuestionData = data.map((e) => {
      const a = new AnswerQuestion();
      a.userUuid = userUuid;
      a.answerUuid = e.answerUuid;
      a.questionUuid = e.questionUuid;
      return a;
    });

    return await AnswerQuestion.getRepository().save<AnswerQuestion>(
      answerQuestionData,
    );
  }
}
