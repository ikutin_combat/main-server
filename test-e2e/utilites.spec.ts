import { Environments } from 'src/utilities/utilities';
import { Utilities } from 'src/utilities/utilities';

const checkObj = {};
for (const p in { ...new Environments() }) {
  checkObj[p] = expect.any(String);
}
describe('testing env', () => {
  it('production', () => {
    process.env.NODE_ENV = 'production';
    const environment = Utilities.getEnvironment();
    expect(environment).toEqual(expect.objectContaining(checkObj));
  });

  it('docker', () => {
    process.env.NODE_ENV = 'docker';
    const environment = Utilities.getEnvironment();
    expect(environment).toEqual(expect.objectContaining(checkObj));
  });
  it('development', () => {
    process.env.NODE_ENV = 'development';
    const environment = Utilities.getEnvironment();
    expect(environment).toEqual(expect.objectContaining(checkObj));
  });
  it('production', () => {
    const environment = Utilities.getEnvironment();
    expect(environment).toEqual(expect.objectContaining(checkObj));
  });
});
