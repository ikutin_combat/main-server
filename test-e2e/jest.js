module.exports = {
  moduleFileExtensions: ['js', 'json', 'ts'],
  rootDir: '.',
  testEnvironment: 'node',
  testRegex: '.spec.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  moduleDirectories: ['node_modules', 'src'],
  moduleNameMapper: {
    'src/(.*)': `${process.cwd()}/src/$1`,
  },
  testEnvironmentOptions: {
    NODE_ENV: 'development',
  },
};
